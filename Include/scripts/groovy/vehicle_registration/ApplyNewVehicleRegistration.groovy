package vehicle_registration
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class ApplyNewVehicleRegistration extends CommonMethods {

	@Given("user is in login page")
	def user_is_in_login_page() {

		//WebUI.openBrowser('')
		//WebUI.navigateToUrl(GlobalVariable.URL)
		//WebUI.maximizeWindow()
		CommonMethods obj = new CommonMethods();
		obj.navigate_to_url();

	}

	@When("user enters username(.*) and password(.*)")
	def user_enters_username_and_password(String username, String password) {

		WebUI.waitForElementVisible(findTestObject('Object Repository/textboxes/textbox.username'), GlobalVariable.MEDIUM_WAIT)
		WebUI.setText(findTestObject('Object Repository/textboxes/textbox.username'), GlobalVariable.USERNAME)
		WebUI.setText(findTestObject('Object Repository/textboxes/textbox.password'), GlobalVariable.PASSWORD)
	}
	@Then("enter OTP")
	def enter_otp() {

		WebUI.delay(GlobalVariable.LONG_WAIT)
	}
	@And("click on sign in")
	def click_on_sign_in() {

		WebUI.click(findTestObject('Object Repository/buttons/button.signin'))
	}
	@Then("verify homepage is displayed")
	def verify_homepage_is_displayed() {

		WebUI.waitForElementVisible(findTestObject('Object Repository/labels/label.homepage'), GlobalVariable.SHORT_WAIT)
	}
}