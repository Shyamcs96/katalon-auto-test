package vehicle_registration
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class ApplyForNewVehicleRegistration extends CommonMethods {

	@Given("user is able to login with valid credentials")
	def user_is_able_to_login_with_valid_username_and_password() {

		CommonMethods obj = new CommonMethods();
		obj.login();
	}

	@When("user navigates to Apply for Vehicle Registration page")
	def user_navigates_to_apply_for_vehicle_registration_page() {

		//Click on Vehicle Registration
		WebUI.waitForElementVisible(findTestObject('Object Repository/links/link.homepage_vehicle_registration'), GlobalVariable.SHORT_WAIT)
		WebUI.click(findTestObject('Object Repository/links/link.homepage_vehicle_registration'))

		//Click on Vehicle Registration
		WebUI.waitForElementVisible(findTestObject('Object Repository/links/link.vehicle_registration.vehicle_registration'), GlobalVariable.SHORT_WAIT)
		WebUI.click(findTestObject('Object Repository/links/link.vehicle_registration.vehicle_registration'))

		//Click on Apply for Vehicle Registration
		WebUI.waitForElementVisible(findTestObject('Object Repository/links/link.vehicle_registration.apply_for_vehicle_registration'), GlobalVariable.SHORT_WAIT)
		WebUI.click(findTestObject('Object Repository/links/link.vehicle_registration.apply_for_vehicle_registration'))
	}

	@Then("verify KRA Individual PIN(.*)")
	def enter_KRA_individual_pin(String KRA_PIN) {

		//Enter KRA Individual PIN
		WebUI.waitForElementVisible(findTestObject('Object Repository/textboxes/textbox.KRA_PIN'), GlobalVariable.SHORT_WAIT)
		WebUI.setText(findTestObject('Object Repository/textboxes/textbox.KRA_PIN'), KRA_PIN)

		//Click on Search icon
		WebUI.click(findTestObject('Object Repository/buttons/button.KRA_PIN.search'))

		//wait for OTP to enter manually(code implementation is pending)
		WebUI.delay(GlobalVariable.MEDIUM_WAIT)

		//click on Next button
		WebUI.click(findTestObject('Object Repository/buttons/button.next'))
	}
	@And("enter valid search parameters(.*) and (.*)")
	def enter_search_parameters(String CUSTOM_ENTRY_NUMBER, String CHASSIS_NUMBER) {

		//Enter Custom Entry Number and Chassis Number
		WebUI.setText(findTestObject('Object Repository/textboxes/textbox.custom_entry_number'), CUSTOM_ENTRY_NUMBER)
		WebUI.setText(findTestObject('Object Repository/textboxes/textbox.chassis_number'), CHASSIS_NUMBER)

		//Click on Enquire
		WebUI.click(findTestObject('Object Repository/buttons/button.enquire'))
	}
	/*
	 @Then("select a record from List of Vehicle(s) which has Vehicle Status as NOT REGISTERED and click on REGISTER")
	 def click_on_register() {
	 //click on Register
	 WebUI.click(findTestObject('Object Repository/buttons/button.register'))
	 }
	 @And("read all Vehicle Details and click on NEXT")
	 def click_on_next() {
	 //wait for label vehicle details
	 WebUI.waitForElementVisible(findTestObject('Object Repository/labels/label.vehicle_details'), GlobalVariable.SHORT_WAIT)
	 //scroll to element Next
	 WebUI.scrollToElement(findTestObject('Object Repository/buttons/button.next'), GlobalVariable.SHORT_WAIT)
	 //Click on Next
	 WebUI.click(findTestObject('Object Repository/buttons/button.next'))
	 }
	 @Then("enter valid details in Collection Arrangements")
	 def enter_valid_details_in_collection_arrangements() {
	 //wait for label collection arrangement
	 WebUI.waitForElementVisible(findTestObject('Object Repository/labels/label.collection_arrangement'), GlobalVariable.SHORT_WAIT)
	 //select checkbox email
	 //select private from use dropdown
	 WebUI.selectOptionByLabel(findTestObject(''), null, false)
	 //enter location of vehicle
	 }
	 @And("enter valid details in Registration Certificate Authorization")
	 def enter_valid_details_in_registration_certificate_authorization() {
	 //scroll down
	 //select logbook collection office from dropdown
	 //enter ID
	 //Click on search
	 }
	 //@And("enter valid details in Plate Authorization")
	 //select plate counter collection office from dropdown
	 //enter ID
	 //Click on search
	 //click on i declare checkbox
	 //@Then("click on SUBMIT")
	 //click on submit
	 //wait for label fees
	 //click on next
	 //wait for label confirm vehicle particulars
	 //click on pay
	 //wait for label choose payment method
	 //select mpesa radio button
	 //click on complete button
	 //wait for label message
	 //click on print
	 //click on download
	 //click on ok
	 */
}