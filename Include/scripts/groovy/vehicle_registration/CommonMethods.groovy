package vehicle_registration

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class CommonMethods {
	def login() {

		WebUI.waitForElementVisible(findTestObject('Object Repository/textboxes/textbox.username'), GlobalVariable.MEDIUM_WAIT)

		//enter valid username and password
		WebUI.setText(findTestObject('Object Repository/textboxes/textbox.username'), GlobalVariable.USERNAME)
		WebUI.setText(findTestObject('Object Repository/textboxes/textbox.password'), GlobalVariable.PASSWORD)

		//wait for OTP to enter manually(code implementation is pending)
		WebUI.delay(GlobalVariable.LONG_WAIT)

		//Click on sign in button
		WebUI.click(findTestObject('Object Repository/buttons/button.signin'))

		//verify homepage
		WebUI.waitForElementVisible(findTestObject('Object Repository/labels/label.homepage'), GlobalVariable.SHORT_WAIT)
	}
}
