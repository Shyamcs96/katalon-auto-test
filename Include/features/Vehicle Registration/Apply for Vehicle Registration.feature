Feature: Apply for New Vehicle Registration
   Verify user is able to Apply for New Vehicle Registration

  Scenario Outline: Apply for new Vehicle Registration
    Given user is able to login with valid credentials
    When user navigates to Apply for Vehicle Registration page
    Then verify KRA Individual PIN<KRA_PIN>
    And enter valid search parameters<CUSTOM_ENTRY_NUMBER> and <CHASSIS_NUMBER>

    #Then select a record from List of Vehicle(s) which has Vehicle Status as NOT REGISTERED and click on REGISTER
    #And read all Vehicle Details and click on NEXT
    #Then enter valid details in Collection Arrangements
    #And enter valid details in Registration Certificate Authorization
    #And enter valid details in Plate Authorization
    #Then click on SUBMIT
    Examples: 
      | KRA_PIN     | CUSTOM_ENTRY_NUMBER | CHASSIS_NUMBER |
      | C000330801A | 2021MSA3742400      | CHIZCCE02000   |
