Feature: Apply for New Vehicle Registration
   Verify user is able to Apply for New Vehicle Registration
  
  Scenario: Login with valid credentials
    Given user is in login page
    When user enters username<username> and password<password>
    Then enter OTP
    And click on sign in
    Then verify homepage is displayed