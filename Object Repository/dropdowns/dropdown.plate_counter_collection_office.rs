<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown.plate_counter_collection_office</name>
   <tag></tag>
   <elementGuidId>ec3fa190-33c7-4203-a74c-27792036efa8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='ctx_CollectionAC_CollectionAC_cProcessInst_cPAuthorization_PlateCounterCol']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='ctx_CollectionAC_CollectionAC_cProcessInst_cPAuthorization_PlateCounterCol']</value>
   </webElementProperties>
</WebElementEntity>
