<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label.fees</name>
   <tag></tag>
   <elementGuidId>eda63f02-97d2-434b-a698-ddc4b9c2e5dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[text()='Fees']/parent::div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[text()='Fees']/parent::div</value>
   </webElementProperties>
</WebElementEntity>
