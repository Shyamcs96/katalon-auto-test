<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label.conformation_of_vehicle_particulars</name>
   <tag></tag>
   <elementGuidId>ff1302cc-a5fd-4ff4-a8b2-490c4b759405</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[text()='Confirmation of Vehicle Particulars']/parent::div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[text()='Confirmation of Vehicle Particulars']/parent::div</value>
   </webElementProperties>
</WebElementEntity>
