<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label.vehicle_details</name>
   <tag></tag>
   <elementGuidId>fcd52bc9-0a95-425d-a2bf-17f5bc007f17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[text()='Vehicle Details']/parent::div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[text()='Vehicle Details']/parent::div</value>
   </webElementProperties>
</WebElementEntity>
