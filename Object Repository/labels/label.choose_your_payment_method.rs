<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label.choose_your_payment_method</name>
   <tag></tag>
   <elementGuidId>b44ece5b-3d3b-4c31-ab92-77922a2f2791</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[text()='Choose your payment Method']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[text()='Choose your payment Method']</value>
   </webElementProperties>
</WebElementEntity>
