<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button.pay.complete</name>
   <tag></tag>
   <elementGuidId>166090ba-51ea-4ad6-85d7-4fcb8ad7b5b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='collapseOne']//button[@class='btn btn-success']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='collapseOne']//button[@class='btn btn-success']</value>
   </webElementProperties>
</WebElementEntity>
