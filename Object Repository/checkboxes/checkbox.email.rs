<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox.email</name>
   <tag></tag>
   <elementGuidId>1ae01518-b4f7-4b9a-bb24-dcafda98cb62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='ctx_CollectionAC_CollectionAC_cProcessInst_cCollectionA_NotificationM']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='ctx_CollectionAC_CollectionAC_cProcessInst_cCollectionA_NotificationM']</value>
   </webElementProperties>
</WebElementEntity>
