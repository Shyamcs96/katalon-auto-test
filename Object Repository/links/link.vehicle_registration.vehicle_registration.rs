<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link.vehicle_registration.vehicle_registration</name>
   <tag></tag>
   <elementGuidId>4ae59a0d-5902-4216-b07d-d2230195acb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//b[text()='Vehicle Registration']/parent::span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//b[text()='Vehicle Registration']/parent::span</value>
   </webElementProperties>
</WebElementEntity>
