<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link.homepage_vehicle_registration</name>
   <tag></tag>
   <elementGuidId>0af45932-fc06-4e8c-b1b3-3aff21027cf8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[text()= 'Vehicle Registration']/parent::a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[text()= 'Vehicle Registration']/parent::a</value>
   </webElementProperties>
</WebElementEntity>
