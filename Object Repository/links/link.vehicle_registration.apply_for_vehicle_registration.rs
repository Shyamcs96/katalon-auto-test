<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link.vehicle_registration.apply_for_vehicle_registration</name>
   <tag></tag>
   <elementGuidId>3037d6fe-5f52-4ca1-9a3b-bd3e41f0a3e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='Apply for Vehicle Registration']/parent::li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='Apply for Vehicle Registration']/parent::li</value>
   </webElementProperties>
</WebElementEntity>
